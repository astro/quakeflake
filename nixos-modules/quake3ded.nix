{ pkgs, self, ... }:
with pkgs;
{
  services.quake3-server = {
    enable = true;
    openFirewall = true;
    baseq3 = self.packages.${pkgs.system}.baseq3;
    extraConfig = ''
      exec "ctf.cfg"
    '';
  };
}

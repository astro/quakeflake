{ pkgs, ... }:
{
  # system
  boot = {
    isContainer = true;
    tmpOnTmpfs = true;
  };
  nix = {
    extraOptions = "experimental-features = nix-command flakes";
    package = pkgs.nixUnstable;
  };
  system.stateVersion = "21.11";

  # network
  networking.useDHCP = false;
  systemd.network.enable = false;
  services.resolved.enable = false;
  networking.firewall.allowedTCPPorts = [ 22 ];
  services.openssh = {
    startWhenNeeded = true;
    permitRootLogin = "prohibit-password";
  };
  users.users.root.openssh.authorizedKeys.keyFiles = [
    ../astro.pub
  ];

  environment.noXlibs = false;
  environment.systemPackages = with pkgs; [
    git tcpdump
  ];
}

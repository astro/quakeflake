{ fetchurl }:
{
  anodm3 = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/anodm3.pk3";
    sha256 = "0c805il3xa10gx4blf1j7pj5qh8qrb0m4rrz8r59vd08wksnx1zs";
  };
  charon3dm13 = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/charon3dm13.pk3";
    sha256 = "0ycvip8hqgkdc0xpwlsd9lwhbgrcsx2rjqmdrqmz7fgjpn91ymdr";
  };
  charon3dm8 = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/charon3dm8.pk3";
    sha256 = "0vs8a505vw1iq7kig0kzgnq4mcj3ymps100dm36h3bd03d0dqxbh";
  };
  ct3ctf1 = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/ct3ctf1.pk3";
    sha256 = "0cjpjkbfmi9a6sq2f6lrkfqqkcl9kfv5gwc48p0accqrv3l9d68n";
  };
  ctctf6 = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/ctctf6.pk3";
    sha256 = "15wv39cc0vhg7qdqwdf7gl7kjr8m335nv4572fixiii0flaaiax8";
  };
  ctf13agony = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/ctf13agony.pk3";
    sha256 = "1d68z0a1zwh3x9p5iyyk97rfvcigkyiwmbswppv73qcmmz5svk61";
  };
  ctf13circle = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/ctf13circle.pk3";
    sha256 = "1x6pd87h37480dn3c7ffdlx8dnkmil1a8ds9k887788z34c6vw8z";
  };
  ctf13dream = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/ctf13dream.pk3";
    sha256 = "01zxgphc66h3r3hs8f7sin5g5pp8na52k6s9ji7mw2hcvr3zgky9";
  };
  ctf13ground_xt = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/ctf13ground_xt.pk3";
    sha256 = "0agvix36afn1q43xix0a678830bcdf9bjm0p7la2dxjr9pycr60x";
  };
  ctf13vast = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/ctf13vast.pk3";
    sha256 = "1ddwpf5frnfnz9a7p266nj8cik7892vf6za48l52cw3ksxq8hrab";
  };
  eg_ctf1 = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/eg_ctf1.pk3";
    sha256 = "1y1ppc3b64ixm87b0q6l9spv3hhy0cr404309y4rdpj2f64wmlfz";
  };
  geit3ctf1 = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/geit3ctf1.pk3";
    sha256 = "1da4j3cf5nh3ldjl8fsig450z107jbq635siaca6qjyqb1jjb06z";
  };
  geit3ctf2 = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/geit3ctf2.pk3";
    sha256 = "00ih8az1wjw16f4c4zx8k3fygbba8faw5s4xz5b8dbivck8wxv5r";
  };
  ironwoodctf = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/ironwoodctf.pk3";
    sha256 = "0g7n1802z27s593hifyf3kxvs8frm57n45cifza76hsik4fxg889";
  };
  lun3dm5 = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/lun3dm5.pk3";
    sha256 = "1l2fjlaayq55yn160bfad7c36pga9yk768waj6xr59018hxnpbw0";
  };
  maps-q3wctf = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/maps-q3wctf.pk3";
    sha256 = "0fa6x53gkr7ms84nci32dil8bgv9wrc90zizmk2hgcic8fsg8w5l";
  };
  nor3ctf1 = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/nor3ctf1.pk3";
    sha256 = "07kmw5jjmvidvjxgnkwxzgkyigglk8cz8niiv9xrmyklsgpnl8x1";
  };
  overkill = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/overkill.pk3";
    sha256 = "0xdjyblfs4yqgv5w73hqbq1amlbhcknfm1xqq53664scd3s7awib";
  };
  pak9hqq37 = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/pak9hqq37.pk3";
    sha256 = "10jrhvq2gfs983xhdxdhcz54rm0268hsdhrshkq9qj90m9ikxmgg";
  };
  q3wpak1 = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/q3wpak1.pk3";
    sha256 = "0y18rk9lsxjw2zkrcijsqqaijqg5irknjbal0dmp701q4vqfg2vy";
  };
  quartzctf1 = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/quartzctf1.pk3";
    sha256 = "1ydmijnxxbsm5b7cyy38rq4d9r0v5g25nar9l7jpv3z7aqjnf2cd";
  };
  schadctf = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/schadctf.pk3";
    sha256 = "04w2297ihkcwy8djjz8km951rzmcfjcivnqd08j987jfhq9nc124";
  };
  storm3tourney8 = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/storm3tourney8.pk3";
    sha256 = "10l835fnrh73d0g4a4yx19v1vgirvs57kavgabm9l40lryrbgm6c";
  };
  xcsv_bq3hi-res = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/xcsv_bq3hi-res.pk3";
    sha256 = "0nq2b63py77bgpafnbzhm20b2jplq5x9b365s38a2kk2bhnx2bx6";
  };
  ztn3dm1 = fetchurl {
    url = "http://share.pedalkickers.de/fps/quake3/baseq3/ztn3dm1.pk3";
    sha256 = "0x1jrs0120mk6xbnc43vyaka8fayyz1w3wi325hqa6knrppl2xbh";
  };
}

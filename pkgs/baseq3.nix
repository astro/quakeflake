{ self, nixpkgs, system }:
with (import nixpkgs {
  inherit system;
  config = { allowUnfree = true; };
});
rec {
  q3iso = fetchurl {
    url = "https://archive.org/download/quake-iii/Quake%20III.iso";
    name = "QuakeIII.iso";
    sha256 = "1qgk5kx7mf75dvlf54x15dpvjvwwh27q6m3cgg8gyiv57hw20cp9";
  };

  isoBaseq3 = stdenv.mkDerivation {
    name = "baseq3";
    src = q3iso;
    nativeBuildInputs = [ p7zip ];
    unpackPhase = ''
      7z x $src
    '';
    installPhase = ''
      cp -a baseq3 $out
    '';
  };

  mappaks = import ./mappaks.nix { inherit fetchurl; };

  baseq3 = runCommand "baseq3" {} ''
    mkdir $out
    # pak0
    ln -s ${isoBaseq3}/* $out/
    # pak1..6
    ln -s ${quake3pointrelease}/baseq3/* $out/
    # mappaks
    ${lib.concatMapStrings (name: ''
      ln -s ${mappaks.${name}} $out/${name}.pk3
    '') (builtins.attrNames mappaks)}
    # cfg
    ln -s ${../q3config_server.cfg} $out/q3config_server.cfg
    ln -s ${../ctf.cfg} $out/ctf.cfg
    ln -s ${../randomctfmaps.cfg} $out/randomctfmaps.cfg
  '';
}

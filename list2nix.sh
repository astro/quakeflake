#!/usr/bin/env bash

echo "{ fetchurl }:"
echo "{"
while read url; do
    url=$(echo "$url"|sed -e 's/\r//')
    filename=$(basename "$url")
    if [[ "$filename" =~ \.pk3$ ]]; then
        name=$(echo "$filename"|sed -e 's/\..*//')
        sha256=$(nix-prefetch-url "$url" 2>/dev/null)
        if [ -n "$sha256" ]; then
            echo "  $name = fetchurl {"
            echo "    url = \"$url\";"
            echo "    sha256 = \"$sha256\";"
            echo "  };"
        fi
    fi
done
echo "}"

{
  description = "Quake 3 Arena Dedicated Server";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/release-22.05";
  };

  outputs = inputs@{ self, nixpkgs }: {

    apps.x86_64-linux = rec {
      q3a = {
        type = "app";
        program = "${self.packages.x86_64-linux.q3a}/bin/q3a";
      };
      default = q3a;
    };

    packages = builtins.mapAttrs (system: _:
      let
        inherit (self.nixosConfigurations) quakeserver;

        remote = "2a01:4f9:4b:39ec::1c";
      in rec {
        rootfs = quakeserver.config.system.build.toplevel;
        container = quakeserver.config.system.build.tarball;

        deploy = with nixpkgs.legacyPackages.${system};
          writeScriptBin "deploy" ''
            #! ${runtimeShell} -e

            nix copy --to ssh://root@${remote} ${rootfs}
            ssh root@${remote} ${rootfs}/bin/switch-to-configuration switch
          '';

        q3a = with nixpkgs.legacyPackages.${system}; buildEnv {
          name = "q3a";
          paths = [
            quake3e
            (runCommand "baseq3" {} ''
              mkdir -p $out/lib/baseq3
              ln -s ${self.packages.${system}.isoBaseq3}/* $out/lib/baseq3/
              ln -s ${quake3pointrelease}/baseq3/* $out/lib/baseq3/
              ln -s ${quake3hires}/baseq3/* $out/lib/baseq3/
            '')
            (writeScriptBin "q3a" ''
              #! ${runtimeShell} -e

              exec $(dirname $0)/quake3e +set fs_basepath $(dirname $0)/../lib
            '')
          ];
        };
      } //
      import ./pkgs/baseq3.nix (inputs // { inherit system; })
    ) { x86_64-linux = true; };

    nixosModules = {
      container = import ./nixos-modules/container.nix;
      quake3ded = import ./nixos-modules/quake3ded.nix;
    };
    
    nixosConfigurations.quakeserver = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      extraArgs = {
        inherit self;
      };
      modules = [
        "${nixpkgs}/nixos/modules/virtualisation/lxc-container.nix"
        self.nixosModules.container
        self.nixosModules.quake3ded
        {
          networking.hostName = "quakeserver";
        }
      ];
    };

  };
}
